develock-el (0.47-3) unstable; urgency=medium

  * Add Python mode (Closes: #771469)
    - add patch 0004-Add-Python-mode.patch
  * Add Groovy mode (Closes: #904687). Thanks to Benedikt Spranger for
    the patch.
  * Update control file with newly supported modes
  * Update Vcs-* fields with Salsa repository

 -- Mehdi Dogguy <mehdi@debian.org>  Thu, 29 Nov 2018 23:36:42 +0100

develock-el (0.47-2) unstable; urgency=medium

  * Enrich added modes (OCaml, Coq and LaTeX) with new checks:
    - long spaces
    - spaces before tabs
    - tab space tab
    - reachable E-mail addresses
    - highlight FIXME and TODO tags
  * Add a debian/emacsen-compat file.
  * Bump Standards-Version to 3.9.8, no changes required.
  * Use canonical and secure URIs for Vcs-* fields.
  * Update copyright years in debian/copyright file.

 -- Mehdi Dogguy <mehdi@debian.org>  Tue, 01 Nov 2016 21:49:34 +0100

develock-el (0.47-1) unstable; urgency=medium

  * New upstream version 0.47
  * Drop 0002-Fix-cpp-compat.patch since it was included upstream
  * Add patch 0002-advising-indent-region-with-correct-prototype.patch
    (Closes: #842603). Thanks to Rémi Vanicat for raising the issue and
    providing a patch!

 -- Mehdi Dogguy <mehdi@debian.org>  Tue, 01 Nov 2016 18:54:08 +0100

develock-el (0.39-2) unstable; urgency=medium

  * Fix compatibility with Emacs's C++ mode (Closes: #771177).

 -- Mehdi Dogguy <mehdi@debian.org>  Mon, 01 Dec 2014 20:59:57 +0100

develock-el (0.39-1) unstable; urgency=low

  * New upstream release.
  * New maintainer (Closes: #654942)
  * Re-do packaging using DH9.
    - Bump Standards-Version to 3.9.3
    - Conform to machine-readable copyright format 1.0
    - Convert to 3.0 (quilt) source format
  * Make develock-el aware of some other modes (Closes: #633564).
    - OCaml modes (caml-mode and tuareg-mode)
    - Coq mode
    - LaTeX mode
  * Add Vcs-{Browser,Git} control fields.

 -- Mehdi Dogguy <mehdi@debian.org>  Wed, 29 Feb 2012 11:52:11 +0100

develock-el (0.36-2) unstable; urgency=low

  * emacsen-startup: Use `global-font-lock-mode' with positive argument.
  (closes: #446083)

 -- OHASHI Akira <bg66@debian.org>  Mon, 04 Aug 2008 10:32:57 +0900

develock-el (0.36-1) unstable; urgency=low

  * New upstream release
  * control.in (Standards-Version): Increase to 3.8.0.

 -- OHASHI Akira <bg66@debian.org>  Tue, 24 Jun 2008 22:55:21 +0900

develock-el (0.35-1) unstable; urgency=low

  * New upstream release

 -- OHASHI Akira <bg66@debian.org>  Fri, 24 Aug 2007 10:19:00 +0900

develock-el (0.34-2) unstable; urgency=low

  * control.in (develock-el/Depends): Use emacs instead of emacs21.

 -- OHASHI Akira <bg66@debian.org>  Tue, 07 Aug 2007 17:53:34 +0900

develock-el (0.34-1) unstable; urgency=low

  * New upstream release
  * copyright (Upstream Authors): Add Oscar Bonilla.

 -- OHASHI Akira <bg66@debian.org>  Fri, 29 Jun 2007 09:26:12 +0900

develock-el (0.31-1) unstable; urgency=low

  * New upstream release

 -- OHASHI Akira <bg66@debian.org>  Fri, 30 Mar 2007 13:32:35 +0900

develock-el (0.30-1) unstable; urgency=low

  * New upstream release

 -- OHASHI Akira <bg66@debian.org>  Tue,  3 Oct 2006 12:18:22 +0900

develock-el (0.29-1) unstable; urgency=low

  * New upstream release (closes: #335611)
  * control.in (Standards-Version): Bump up to 3.7.2.
  (Build-Depends-Indep): Abolish.
  (Build-Depends): New field.

 -- OHASHI Akira <bg66@debian.org>  Mon, 25 Sep 2006 19:41:55 +0900

develock-el (0.27-2) unstable; urgency=low

  * rules: Use cdbs to build.
  * control.in: New file for cdbs.
  * emacsen-install: Rewrite to a new de facto standard format.
  (closes: #330784)
  * emacsen-startup: Use debian-emacs-flavor instead of flavor.

 -- OHASHI Akira <bg66@debian.org>  Tue, 21 Feb 2006 14:02:29 +0900

develock-el (0.27-1) unstable; urgency=low

  * New upstream release
  * emacsen-startup: Support emacs-snapshot.

 -- OHASHI Akira <bg66@debian.org>  Thu,  9 Feb 2006 14:44:50 +0900

develock-el (0.26-2) unstable; urgency=low

  * control (Standards-Version): Increased to 3.6.2.
  * control (develock-el/Depends): Use `emacsen' instead of
  `xemacs21'. (closes: #321994)

 -- OHASHI Akira <bg66@debian.org>  Sat, 10 Dec 2005 09:34:16 +0900

develock-el (0.26-1) unstable; urgency=low

  * New upstream release (closes: #309404)

 -- OHASHI Akira <bg66@debian.org>  Wed, 18 May 2005 10:17:28 +0900

develock-el (0.25-1) unstable; urgency=low

  * New upstream release

 -- OHASHI Akira <bg66@debian.org>  Thu, 10 Mar 2005 11:13:49 +0900

develock-el (0.23-5) unstable; urgency=low

  * emacsen-install: Fix for the broken installation. 

 -- OHASHI Akira <bg66@debian.org>  Fri, 13 Aug 2004 18:03:50 +0900

develock-el (0.23-4) unstable; urgency=low

  * emacsen-startup: Don't add uncompiled files to the load-path. 

 -- OHASHI Akira <bg66@debian.org>  Thu, 12 Aug 2004 12:26:16 +0900

develock-el (0.23-3) unstable; urgency=low

  * emacsen-startup: Use `debian-pkg-add-load-path-item' and add uncompiled
  files to the load-path.
  * emacsen-install: Enable SITEFLAG and output a log to tempfile.

 -- OHASHI Akira <bg66@debian.org>  Fri,  6 Aug 2004 11:24:53 +0900

develock-el (0.23-2) unstable; urgency=low

  * control (Depends): Don't depend `emacs20'. (closes: #232749)

 -- OHASHI Akira <bg66@debian.org>  Mon, 16 Feb 2004 14:04:34 +0900

develock-el (0.23-1) unstable; urgency=low

  * New upstream release

 -- OHASHI Akira <bg66@debian.org>  Fri,  5 Dec 2003 16:02:04 +0900

develock-el (0.22-1) unstable; urgency=low

  * New upstream release
  * control (Standards-Version): Increased to 3.6.1.

 -- OHASHI Akira <bg66@debian.org>  Thu, 13 Nov 2003 11:52:25 +0900

develock-el (0.21-1) unstable; urgency=low

  * Initial Release. (closes: #204658)

 -- OHASHI Akira <bg66@debian.org>  Tue, 19 Aug 2003 22:32:48 +0900
